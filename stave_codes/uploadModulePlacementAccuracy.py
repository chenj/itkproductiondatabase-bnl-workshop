
#uploadModulePlacementAccuracy.py
#From the final survey directory, get the placement accuracy for all modules on one side of a stave
#attach the calibration file for the final survey
#written by Jiayi Chen, last update 4/3/2019

from __future__ import print_function #import python3 printer if python2 is used

if __name__ == '__main__':
    from __path__ import updatePath
    updatePath()

import numpy as np
import collections, argparse, os, json,sys
import ReadSurvey as RS

#path=os.path.abspath(os.path.join('./../', os.pardir))
#sys.path.append(path)
#from utilities.databaseUtilities import commands
from itk_pdb.databaseUtilities import Colours, INFO, PROMPT, WARNING, ERROR, STATUS
from itk_pdb.dbAccess import ITkPDSession
from LoadedStave_class import FindComp, STAVE
from datetime import date
from requests_toolbelt.multipart.encoder import MultipartEncoder


#resolve python2 raw_input() & python3 input() problem
if sys.version_info>(3,0):
    raw_input=input #written scripts using raw_input()


class MPAtest(object):
    def __init__(self,LOCALNAME=None,institution='BU',side="RHS",ITkPDSession=None):
        self.localname=LOCALNAME
        self.institution=institution
        self.stave_side=side #run number
        self.ITkPDSession=ITkPDSession
        self.stave_code,self.slotIDs=self.findStaveINFO()
        self.json=self.initiateSurvey()
        self.test_run_id=None

    def findStaveINFO(self):
        Stave=STAVE(LOCALNAME=self.localname,dir=None,STAGE='assembly',ITkPDSession=self.ITkPDSession) #since day2 start with assembly stage
        component=Stave.StaveINFO()
        return Stave.code,Stave.slotIDs


    #initialize the JSON (needs to be changed if the test format is changed) -- should try using CMD 'getTestType' or 'getTestTypeByCode'
    def initiateSurvey(self):
        DTO={
        "component": self.stave_code,
        "testType": "SURVEY-SUMMARY",
        "institution": self.institution,
        "runNumber":self.stave_side,
        "date": date.today().strftime("%d.%m.%Y"),
        "passed": False,
        "problems": True,
        "properties": {},
        "results":{"PASS":[],"FAIL-X":[],"FAIL-Y":[],"FAIL-XY":[]}}
        return DTO


    #fill in 'results' in the JSON
    def fillResults(self,dir=None,modules=[2]):

        #check if properties if filled (only fill in once)
        fillFiducial=False

        #initiate test parameters
        Passed=True
        num_fail=0

        for module in modules:
            #open the module survey directory and find the module survey file at: 'ModulePlacement/13/Module_13.txt'
            directory=dir+'ModulePlacement/'+str(module)+'/'
            survey = RS.TheSurveys("Module"+str(module), "Module_"+str(module) +".txt",directory)

            #print failed module info
            survey.PrintTheFailures()

            #fill fiducail property once (assume final survey uses the same fiducial)
            if not fillFiducial:
                for line in survey.lines:
                    if "FiducialMark" in line:
                        fiducial=line[line.find("=")+3:-3]
                        self.json["properties"]["FIDUCIAL"]=fiducial
                fillFiducal=True

            #initiate result {childrenproperties:***,value:{A:[x,y],B:[],C:[],D:[]}}
            result={}

            #find the slot_id for the MODULE
            slot=module
            if self.stave_side=='J':
                slot+=14
            result["childParentRelation"]=self.slotIDs[slot]

            #use after bridge removal survey (but this should be changed, as we should upload Final Survey result)
            if 'ABR' in survey.stages:

                #survey.DeltaXY already formated in the right way for database
                result["value"]=survey.DeltaXY['ABR']
            else:
                print 'Stage ABR not in the stages, here are the stages:',survey.stages
                while True:
                    try:
                        stage=raw_input('enter the stage you want:')
                        result["value"]=survey.DeltaXY[stage]
                        break
                    except KeyError:
                        print 'wrong input, please insert one of the following stages:'
                        print survey.stages

            #fill in parameters (4 categories 'PASSED', 'FAIL-X','Y', 'X&Y')
            #fill in parameters (4 categories 'PASSED', 'FAIL-X','Y', 'X&Y'); find out which category this module falls into and fill in results

            if survey.passed==True:
                self.json["results"]["PASS"].append(result)
            else:
                Passed=False
                num_fail+=1
                if survey.failX==True and survey.failY==False:
                    self.json["results"]["FAIL-X"].append(result)
                elif survey.failX==False and survey.failY==True:
                    self.json["results"]["FAIL-Y"].append(result)
                else:
                    self.json["results"]["FAIL-XY"].append(result)

        #loop over modules finished

        #fill in the rest of the properties
        self.json["passed"]=Passed
        self.json["problem"]=not Passed
        self.json["properties"]['FAILURE']=num_fail

        dx=[]
        dy=[]
        for param in self.json["results"].keys():
            for module in self.json["results"][param]:

        #initiate delta_x and delta_y arrays to calculate RMS for all modules all corners
        dx=[]
        dy=[]

        #fill in RMS in properties
        for param in self.json["results"].keys(): #parameters are PASS, FAIL, FAIL-X, FAIL-Y
            for module in self.json["results"][param]: #json already has each module's slot id in the four category

                value=module["value"]
                for corner in value.keys():
                    dx.append(value[corner][0])
                    dy.append(value[corner][1])
        self.json["properties"]['RMS-X']=float('%.3g' % rms(dx))
        self.json["properties"]['RMS-Y']=float('%.3g' % rms(dy))

    def uploadTest(self,directory):
        self.test_run_id=self.ITkPDSession.doSomething(action='uploadTestRunResults',method='POST',data=self.json)['testRun']['id']
        #commands['uploadTestRunResults'].run(**test.json)
        print('attaching the calibration file' + directory + 'CalibrationResults.ini')

    #upload JSON and the calibration file
    def uploadTest(self,directory):

        #upload and get the test run ID to attach calibration file
        self.test_run_id=self.ITkPDSession.doSomething(action='uploadTestRunResults',method='POST',data=self.json)['testRun']['id']

        INFO('attaching the calibration file' + directory + 'CalibrationResults.ini')

        #upload attachment
        fields = {  'data': ('CalibrationResults.ini', open(directory+'CalibrationResults.ini', 'rb')),
                     'type': 'file',
                     'testRun': self.test_run_id,
                     'title': 'Calibration File', # Or you could give it a real title
                     'description': 'sample description'  }
        data = MultipartEncoder(fields = fields)
        self.ITkPDSession.doSomething(action ='createTestRunAttachment', method = 'POST', data = data)


#calculate the RMS of an array
def rms(a):
    blah=0.
    for num in a:
        blah += num**2
    rms=np.sqrt(blah/float(len(a)))
    return rms


#from Matt Basso's registerComponent.py
def getYesOrNo(prompt):
    PROMPT(prompt)

    while True:
        # Get our input
        response = raw_input().strip() # changed input()-->raw_input
        # Skip empty inputs
        if response == '':
            continue
        # If yes, return True
        elif response in ['y', 'Y', 'yes', 'Yes', 'YES', '1','true','True']:
            return True
        # If no, return False
        elif response in ['n', 'N', 'no', 'No', 'NO', '0','false','False']:
            return False
        # Else the input is invalid and ask again
        else:
            del response

            print('Invalid input. Please enter \'y/Y\', or \'n/N\':')

            WARNING('Invalid input. Please enter \'y/Y\', or \'n/N\':')

            continue

def main(args):
    print('')
    print('*************************************************************************')
    print('* *                                                                 *   *')
    print('*                            uploadMPA.py --JiayiChen                 *')
    print('* *                                                                 *   *')
    print('*************************************************************************')
    print('')


    directory=args.directory
    while not os.path.exists(directory):
        print 'calibration path (%s) not found' %directory
        directory=raw_input("Please enter the path to calibtration again:")
    if directory[-1]!='/':
        directory+='/'

    directory=args.directory #calibration file

    #check if directory exist
    while not os.path.exists(directory):
        print 'calibration path (%s) not found' %directory
        directory=raw_input("Please enter the path to calibtration again:")

    #needs directory string ends with a '/'
    if directory[-1]!='/':
        directory+='/'

    #get STAVE local name

    localname=raw_input('please give the STAVE\'s local name:')

    #start a ITkPDsession
    session = ITkPDSession()
    session.authenticate()


    test=MPAtest(LOCALNAME=localname,ITkPDSession=session)
    #upload final survay (which is supposed to have all module survey)
    modu_positions=modu_positions=np.arange(2,14,1) #electrical stave 2-13
    test.fillResults(dir=directory,modules=modu_positions)
    print('***Filled DTOin*****')
    print(test.json)
    if args.command=='upload':
        getYesOrNo('Are you sure you want to upload the above test?(yes/no)')

    #initiate MPAtest Object
    test=MPAtest(LOCALNAME=localname,ITkPDSession=session)

    #define module position array
    modu_positions=np.arange(2,14,1) #electrical stave 2-13, real stave change to: modu_positions=np.arange(1,15,1)

    #fill in JSON to be uploaded to the database
    test.fillResults(dir=directory,modules=modu_positions)
    INFO('***Filled DTOin*****')
    print(test.json)

    #if the command is to upload (not testing), upload!
    if args.command=='upload':

        #confirm upload
        getYesOrNo('Are you sure you want to upload the above test?(yes/no)')

        #needs the directory path to find the calibration file and upload the cali file too

        test.uploadTest(directory)
        print 'finished uploading'

if __name__=='__main__':
    parser=argparse.ArgumentParser(description='upload Module Placement Accuracy test to ITk PD')
    parser.add_argument('command',type=str,choices=['testing','upload'],help='testing: will show the json only; upload: will upload json to PD')
    parser.add_argument('--directory',type=str, help='directory that has the final survey results')
    args=parser.parse_args()
    try:
        main(args)

    parser.add_argument('--directory',type=str, help='directory that has the final survey results (\'ElectricalStaveFinalSurvey\')')
    args=parser.parse_args()

    try:
        main(args)

    except KeyboardInterrupt:
        print ''
        print 'Exectution terminated.'
        print 'Finished with error.'
        exit()
