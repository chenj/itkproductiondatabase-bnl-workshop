Module Codes
============

Scripts for interfacing content related to strip modules (e.g., ITSDAQ tests) with the ITk Production Database (ITkPD). It is not recommended to be using uploadITSDAQTests_RETIRED.py or ITSDAQUploadGui_RETIRED.py as they are obsolete and will be removed in the future. The following discussion may or may not be relevant to their use cases.

uploadITSDAQTests.py
--------------------

A command line script for uploading ITSDAQ results from a local machine to the ITkPD. General use is:

```
./uploadITSDAQTests.py <$(SCTDAQ_VAR) location> [--getConfirm] [--uploadFiles] [--psPath <ps folder path>] [--cfgPath <config folder path>] [--recursionDepth <depth>]
```

Where `--getConfirm` will ask for a confirmation of each file to be uploaded, `--uploadFiles` will look for *.pdf, *.det, *.trim, and *.mask files associated with each test and upload them, `--psPath` denotes the location of the $(SCTDAQ_VAR)/ps/ location or equivalent (in case the location where the ps output is stored is not in the ITSDAQ installation directory, $(SCTDAQ_VAR)), `--cfgPath` denotes the location of the $(SCTDAQ_VAR)/config/ location or equivalent (for the same reason as ps; in these cases, <$(SCTDAQ_VAR) location> is assumed to point to $(SCTDAQ_VAR)/results/), and `--recursionDepth` denotes how deep you wish to search for files in any of the aformentioned directories (default = 0 := top level). Running `./uploadITSDAQTests.py --help` gives a print-out similar to the above.

ITSDAQUploadGui.py
------------------

A GUI interface for uploading ITSDAQ results from a local machine to the ITkPD. General use is:

```
./ITSDAQUploadGui.py
```

Where the arguments within the GUI match those as uploadITSDAQTests.py. For a tutorial on either script, please refer to this [talk](https://indico.cern.ch/event/808725/contributions/3385800/attachments/1828778/2997008/190410_Basso_ATLASUpgradeWeek_ITkPDQualificationTaskSummary_EDITED.pdf). Note: for ease of debugging the GUI, one should set `_DEBUG = True` at the top of ITSDAQUploadGui.py, which will provide a print-out of the functions the GUI is entering/exiting.
