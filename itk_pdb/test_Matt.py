
from itk_pdb.dbAccess import ITkPDSession
session = ITkPDSession()
session.authenticate()
list_of_components = session.doSomething('listComponents', 'GET', {'project': 'S', 'componentType': 'HYBRID'})
detailed_list_of_components = session.doSomething('getComponentBulk', 'GET', {'component': [component['code'] for component in list_of_components]})
detailed_list_of_components_2 = []
for i in range(10000):
	detailed_list_of_components_2 += detailed_list_of_components

from ReportingClasses import CutFlow, DictCut

cut0 = DictCut('properties.code', '==', 'LOCALNAME')
cut1 = DictCut('properties.value', 'RE', r'TO2R0_.*_VAN')
cut2 = DictCut('reworked', 'IN', [False, 'shiba_inu'])

cutflow = CutFlow()
cutflow.addCut(cut0)
cutflow.addCut(cut1)
cutflow.addCut(cut2)

import time
t = time.time()
filtered_components = [component for component in detailed_list_of_components_2 if cutflow.apply(component)]
print time.time()-t
print len(filtered_components)
